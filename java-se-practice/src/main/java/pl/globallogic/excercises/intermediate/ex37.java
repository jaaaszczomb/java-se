package pl.globallogic.excercises.intermediate;

public class ex37 {

    public static void main(String[] args) {
        ex37Wall wall1 = new ex37Wall("West");
        ex37Wall wall2 = new ex37Wall("East");
        ex37Wall wall3 = new ex37Wall("South");
        ex37Wall wall4 = new ex37Wall("North");
        Ceiling ceiling = new Ceiling(12, 55);
        Bed bed = new Bed("Modern", 4, 3, 2, 1);
        Lamp lamp = new Lamp("Classic", false, 75);
        Bedroom bedRoom = new Bedroom("YOUR NAME HERE", wall1, wall2, wall3, wall4, ceiling, bed, lamp);
        bedRoom.makeBed();
        bedRoom.getLamp().turnOn();
    }

}

class Lamp {
    private final String style;
    private final boolean battery;
    private final int globRating;

    public Lamp(String style, boolean battery, int globRating) {
        this.style = style;
        this.battery = battery;
        this.globRating = globRating;
    }

    public void turnOn() {
        System.out.println("Lamp -> Turning on");
    }

    public String getStyle() {
        return style;
    }

    public boolean isBattery() {
        return battery;
    }

    public int getGlobRating() {
        return globRating;
    }
}

class Bed {
    private final String style;
    private final int pillows;
    private final int height;
    private final int sheets;
    private final int quilt;

    public Bed(String style, int pillows, int height, int sheets, int quilt) {
        this.style = style;
        this.pillows = pillows;
        this.height = height;
        this.sheets = sheets;
        this.quilt = quilt;
    }

    public void make() {
        System.out.println("Bed -> Making | ");
    }

    public String getStyle() {
        return style;
    }

    public int getPillows() {
        return pillows;
    }

    public int getHeight() {
        return height;
    }

    public int getSheets() {
        return sheets;
    }

    public int getQuilt() {
        return quilt;
    }
}

class Ceiling {
    private final int height;
    private final int paintedColor;

    public Ceiling(int height, int paintedColor) {
        this.height = height;
        this.paintedColor = paintedColor;
    }

    public int getHeight() {
        return height;
    }

    public int getPaintedColor() {
        return paintedColor;
    }
}

class ex37Wall {
    private final String direction;

    public ex37Wall(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}

class Bedroom {
    private final String name;
    private final ex37Wall wall1;
    private final ex37Wall wall2;
    private final ex37Wall wall3;
    private final ex37Wall wall4;
    private final Ceiling ceiling;
    private final Bed bed;
    private final Lamp lamp;

    public Bedroom(String name, ex37Wall wall1, ex37Wall wall2, ex37Wall wall3, ex37Wall wall4, Ceiling ceiling, Bed bed, Lamp lamp) {
        this.name = name;
        this.wall1 = wall1;
        this.wall2 = wall2;
        this.wall3 = wall3;
        this.wall4 = wall4;
        this.ceiling = ceiling;
        this.bed = bed;
        this.lamp = lamp;
    }

    public void makeBed() {
        System.out.print("Bedroom -> Making bed | ");
        bed.make();
    }

    public Lamp getLamp() {
        return lamp;
    }
}