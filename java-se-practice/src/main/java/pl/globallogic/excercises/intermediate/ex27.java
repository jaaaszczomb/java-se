package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class ex27 {

    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
    }

    public static void inputThenPrintSumAndAverage() {
        Scanner scammer = new Scanner(System.in);

        int sum = 0;
        int count = 0;

        while (true) {
            boolean hasNextInt = scammer.hasNextInt();
            if (hasNextInt) {
                int number = scammer.nextInt();
                sum += number;
                count++;
            }
            else break;
        }

        scammer.close();

        long avg = Math.round((double) sum / count);
        System.out.println("SUM = " + sum + " AVG = " + avg);
    }

}
