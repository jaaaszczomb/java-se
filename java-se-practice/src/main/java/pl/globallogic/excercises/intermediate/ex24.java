package pl.globallogic.excercises.intermediate;

public class ex24 {

    public static void main(String[] args) {
        System.out.println(getDigitCount(0));
        System.out.println(getDigitCount(123));
        System.out.println(getDigitCount(-12));
        System.out.println(getDigitCount(5200));

        System.out.println(reverse(-121));
        System.out.println(reverse(1212));
        System.out.println(reverse(1234));
        System.out.println(reverse(100));

        numberToWords(123);
        numberToWords(1010);
        numberToWords(1000);
        numberToWords(-12);
    }

    public static void numberToWords(int number) {
        if (number < 0) System.out.println("Invalid Value");
        else {
            int rNum = reverse(number);
            int digitCountOG = getDigitCount(number);
            int digitCountRev = getDigitCount(rNum);

            while (rNum > 0) {
                int lastDigit = rNum % 10;
                rNum /= 10;

                switch (lastDigit) {
                    case 0 -> System.out.print("Zero ");
                    case 1 -> System.out.print("One ");
                    case 2 -> System.out.print("Two ");
                    case 3 -> System.out.print("Three ");
                    case 4 -> System.out.print("Four ");
                    case 5 -> System.out.print("Five ");
                    case 6 -> System.out.print("Six ");
                    case 7 -> System.out.print("Seven ");
                    case 8 -> System.out.print("Eight ");
                    case 9 -> System.out.print("Nine ");
                }
            }

            for (int i = 0; i < digitCountOG - digitCountRev; i++) System.out.print("Zero ");
            System.out.println();
        }
    }

    public static int reverse(int number) {
        int rNum = 0;
        while (number != 0) {
            rNum = rNum * 10 + number % 10;
            number /= 10;
        }
        return rNum;
    }

    public static int getDigitCount(int number) {
        if (number < 0) return -1;
        if (number == 0) return 1;
        int count = 0;
        while (number > 0) {
            count++;
            number /= 10;
        }
        return count;
    }
}
