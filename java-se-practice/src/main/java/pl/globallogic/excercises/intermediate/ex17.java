package pl.globallogic.excercises.intermediate;

public class ex17 {

    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(252));
        System.out.println(sumFirstAndLastDigit(257));
        System.out.println(sumFirstAndLastDigit(0));
        System.out.println(sumFirstAndLastDigit(5));
        System.out.println(sumFirstAndLastDigit(-10));
    }
    public static int sumFirstAndLastDigit(int number) {
        if (number < 0)  return -1;
        int lDigit = number % 10;
        int fDigit = 0;
        while (number > 0) {
            fDigit = number;
            number /= 10;
        }
        return fDigit + lDigit;
    }

}
