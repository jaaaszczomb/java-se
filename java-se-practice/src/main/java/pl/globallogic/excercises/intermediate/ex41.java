package pl.globallogic.excercises.intermediate;

import java.util.Arrays;
import java.util.Scanner;

public class ex41 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter size of array: ");
        int arraySize = scanner.nextInt();
        int[] ogInts = getIntegers(arraySize);

        System.out.println("\nOriginal Array: ");
        printArray(ogInts);

        int[] sortedInts = sortIntegers(ogInts);

        System.out.println("\nSorted array: ");
        printArray(sortedInts);
    }

    public static int[] getIntegers(int intsSize) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[intsSize];
        System.out.println("Enter " + intsSize + " integers:");
        for (int i = 0; i < intsSize; i++) array[i] = scanner.nextInt();
        return array;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) System.out.println("Element " + i + " contents " + array[i]);
    }

    public static int[] sortIntegers(int[] array) {
        int[] sortedArray = Arrays.copyOf(array, array.length);
        Arrays.sort(sortedArray);
        for (int i = 0; i < sortedArray.length / 2; i++){
            int tmp = sortedArray[i];
            sortedArray[i] = sortedArray[sortedArray.length - 1 - i];
            sortedArray[sortedArray.length - 1 - i] = tmp;
        }
        return sortedArray;
    }

}
