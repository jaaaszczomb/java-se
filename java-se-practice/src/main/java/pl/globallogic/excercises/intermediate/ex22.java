package pl.globallogic.excercises.intermediate;

public class ex22 {

    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25, 15));
        System.out.println(getGreatestCommonDivisor(12, 30));
        System.out.println(getGreatestCommonDivisor(9, 18));
        System.out.println(getGreatestCommonDivisor(81, 153));
    }
    public static int getGreatestCommonDivisor(int first, int second) {
        if (first < 10 || second < 10) return -1;
        while (second != 0) {
            int tmp = second;
            second = first % second;
            first = tmp;
        }
        return first;
    }

}
