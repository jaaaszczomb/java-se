package pl.globallogic.excercises.intermediate;

public class ex19 {

    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12, 23));
        System.out.println(hasSharedDigit(9, 99));
        System.out.println(hasSharedDigit(15, 55));
    }
    public static boolean hasSharedDigit(int first, int second) {
        if (first < 10 || first > 99 || second < 10 || second > 99) return false;

        int fDigit1 = first % 10;
        int fDigit2 = first / 10;
        int sDigit1 = second % 10;
        int sDigit2 = second / 10;

        return (fDigit1 == sDigit1 || fDigit1 == sDigit2 || fDigit2 == sDigit1 || fDigit2 == sDigit2);
    }

}
