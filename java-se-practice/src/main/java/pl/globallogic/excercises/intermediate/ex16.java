package pl.globallogic.excercises.intermediate;

public class ex16 {

    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));
    }
    public static boolean isPalindrome(int number) {
        int ogNumber = number;
        int revNumber = 0;

        while (number != 0) {
            int lastDigit = number % 10;
            revNumber = revNumber * 10 + lastDigit;
            number /= 10;
        }

        return ogNumber == revNumber;
    }


}
