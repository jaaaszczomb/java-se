package pl.globallogic.excercises.intermediate;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

class Album {
    private final String name;
    private final String artist;
    private final ArrayList<Song> songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<>();
    }

    public boolean addSong(String title, double duration) {
        if (findSong(title) == null) {
            this.songs.add(new Song(title, duration));
            return true;
        }
        return false;
    }

    public Song findSong(String title) {
        for (Song song : songs) if (song.getTitle().equals(title)) return song;
        return null;
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist) {
        int index = trackNumber - 1;
        if (index >= 0 && index < songs.size()) {
            playlist.add(songs.get(index));
            return true;
        }
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playlist) {
        Song song = findSong(title);
        if (song != null) {
            playlist.add(song);
            return true;
        }
        return false;
    }
}

class Song {
    private final String title;
    private final double duration;

    public Song(String title, double duration) {
        this.title = title;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title + ": " + duration;
    }
}

public class ex45 {
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        ArrayList<Album> albums = new ArrayList<>();
        Album album = new Album("Stormbringer", "Deep Purple");
        album.addSong("Stormbringer", 4.6);
        album.addSong("Love don't mean a thing", 4.22);
        album.addSong("Holy man", 4.3);
        album.addSong("Hold on", 5.6);
        album.addSong("Lady double dealer", 3.21);
        album.addSong("You can't do it right", 6.23);
        album.addSong("High ball shooter", 4.27);
        album.addSong("The gypsy", 4.2);
        album.addSong("Soldier of fortune", 3.13);
        albums.add(album);

        album = new Album("For those about to rock", "AC/DC");
        album.addSong("For those about to rock", 5.44);
        album.addSong("I put the finger on you", 3.25);
        album.addSong("Lets go", 3.45);
        album.addSong("Inject the venom", 3.33);
        album.addSong("Snowballed", 4.51);
        album.addSong("Evil walks", 3.45);
        album.addSong("C.O.D.", 5.25);
        album.addSong("Breaking the rules", 5.32);
        album.addSong("Night of the long knives", 5.12);
        albums.add(album);

        LinkedList<Song> playList = new LinkedList<Song>();
        albums.get(0).addToPlayList("You can't do it right", playList);
        albums.get(0).addToPlayList("Holy man", playList);
        albums.get(0).addToPlayList("Speed king", playList); // Does not exist
        albums.get(0).addToPlayList(9, playList);
        albums.get(1).addToPlayList(3, playList);
        albums.get(1).addToPlayList(2, playList);
        albums.get(1).addToPlayList(24, playList); // There is no track 24

        playList(playList);
    }

    private static void playList(LinkedList<Song> playlist) {
        ListIterator<Song> iterator = playlist.listIterator();
        boolean forward = true;
        boolean quit = false;

        while (!quit) {
            int choice;
            printMenu();
            choice = -1;

            while (choice < 0 || choice > 6) {
                System.out.print("Enter your choice: ");
                if (scanner.hasNextInt()) choice = scanner.nextInt();
                else {
                    System.out.println("Invalid input. Please enter a number.");
                    scanner.nextLine();
                }
            }

            switch (choice) {
                case 0 -> {
                    System.out.println("Playlist is over.");
                    quit = true;
                }
                case 1 -> {
                    if (!forward) {
                        if (iterator.hasNext()) iterator.next();
                        forward = true;
                    }
                    if (iterator.hasNext()) System.out.println("Now playing: " + iterator.next().toString());
                    else {
                        System.out.println("Reached the end of the playlist.");
                        forward = false;
                    }
                }
                case 2 -> {
                    if (forward) {
                        if (iterator.hasPrevious()) iterator.previous();
                        forward = false;
                    }
                    if (iterator.hasPrevious()) System.out.println("Now playing: " + iterator.previous().toString());
                    else {
                        System.out.println("Reached the beginning of the playlist.");
                        forward = true;
                    }
                }
                case 3 -> {
                    if (forward) {
                        if (iterator.hasPrevious()) {
                            System.out.println("Now replaying: " + iterator.previous().toString());
                            forward = false;
                        } else System.out.println("Reached the beginning of the playlist.");
                    } else {
                        if (iterator.hasNext()) {
                            System.out.println("Now replaying: " + iterator.next().toString());
                            forward = true;
                        } else System.out.println("Reached the end of the playlist.");
                    }
                }
                case 4 -> printList(playlist);
                case 5 -> printMenu();
                case 6 -> {
                    if (!playlist.isEmpty()) {
                        iterator.remove();
                        if (iterator.hasNext()) System.out.println("Now playing: " + iterator.next().toString());
                        else if (iterator.hasPrevious()) System.out.println("Now playing: " + iterator.previous().toString());
                        else System.out.println("The playlist is empty.");
                    }
                }
            }
        }
    }

    private static void printMenu() {
        System.out.println("Available actions:\npress");
        System.out.println("0 - to quit\n" +
                "1 - to play next song\n" +
                "2 - to play previous song\n" +
                "3 - to replay the current song\n" +
                "4 - list songs in the playlist\n" +
                "5 - print available actions\n" +
                "6 - delete current song from playlist");
    }

    private static void printList(LinkedList<Song> playlist) {
        ListIterator<Song> iterator = playlist.listIterator();
        System.out.println("===================================");
        while (iterator.hasNext()) System.out.println(iterator.next().toString());
        System.out.println("===================================");
    }
}
