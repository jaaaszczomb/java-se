package pl.globallogic.excercises.intermediate;

public class ex26 {

    public static void main(String[] args) {
        System.out.println(getLargestPrime(21));
        System.out.println(getLargestPrime(217));
        System.out.println(getLargestPrime(0));
        System.out.println(getLargestPrime(45));
        System.out.println(getLargestPrime(-1));
    }

    public static int getLargestPrime(int number) {
        if (number <= 1) return -1;
        int largestP = -1;
        for (int i = 2; i <= number; i++) {
            if (number % i == 0) {
                boolean isP = true;
                for (int j = 2; j <= Math.sqrt(i); j++) {
                    if (i != j && i % j == 0) {
                        isP = false;
                        break;
                    }
                }
                if (isP) largestP = i;
            }
        }
        return largestP;
    }

}
