package pl.globallogic.excercises.intermediate;

public class ex47 {
    public static void main(String[] args) {
        Node node = new Node("5");
        MyLinkedList linkedList = new MyLinkedList(node);
        linkedList.addItem(new Node("2"));
        linkedList.addItem(new Node("8"));
        linkedList.addItem(new Node("1"));
        linkedList.traverse(linkedList.getRoot());

        SearchTree tree = new SearchTree(node);
        tree.addItem(new Node("2"));
        tree.addItem(new Node("8"));
        tree.addItem(new Node("1"));
        tree.traverse(tree.getRoot());
    }
}

abstract class ListItem {
    protected ListItem rightLink = null;
    protected ListItem leftLink = null;
    protected Object value;

    public ListItem(Object value) {
        this.value = value;
    }

    abstract ListItem next();
    abstract ListItem setNext(ListItem item);
    abstract ListItem previous();
    abstract ListItem setPrevious(ListItem item);
    abstract int compareTo(ListItem item);

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}

class Node extends ListItem {
    public Node(Object value) {
        super(value);
    }

    @Override
    ListItem next() {
        return this.rightLink;
    }

    @Override
    ListItem setNext(ListItem item) {
        this.rightLink = item;
        return this.rightLink;
    }

    @Override
    ListItem previous() {
        return this.leftLink;
    }

    @Override
    ListItem setPrevious(ListItem item) {
        this.leftLink = item;
        return this.leftLink;
    }

    @Override
    int compareTo(ListItem item) {
        if (item != null) return ((String) super.getValue()).compareTo((String) item.getValue());
        else return -1;
    }
}

interface NodeList {
    ListItem getRoot();
    boolean addItem(ListItem item);
    boolean removeItem(ListItem item);
    void traverse(ListItem root);
}

class MyLinkedList implements NodeList {
    private ListItem root = null;

    public MyLinkedList(ListItem root) {
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return root;
    }

    @Override
    public boolean addItem(ListItem newItem) {
        if (this.root == null) {
            this.root = newItem;
            return true;
        }

        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparison = currentItem.compareTo(newItem);
            if (comparison < 0) {
                if (currentItem.next() != null) currentItem = currentItem.next();
                else {
                    currentItem.setNext(newItem);
                    return true;
                }
            } else if (comparison > 0) {
                if (currentItem.previous() != null) currentItem = currentItem.previous();
                else {
                    currentItem.setPrevious(newItem);
                    return true;
                }
            } else {
                System.out.println(newItem.getValue() + " is already in the list.");
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        if (item != null) System.out.println("Deleting item " + item.getValue());
        ListItem currentItem = this.root;
        ListItem parentItem = null;
        while (currentItem != null) {
            int comparison = currentItem.compareTo(item);
            if (comparison < 0) {
                parentItem = currentItem;
                currentItem = currentItem.next();
            } else if (comparison > 0) {
                parentItem = currentItem;
                currentItem = currentItem.previous();
            } else {
                performRemoval(currentItem, parentItem);
                return true;
            }
        }
        return false;
    }

    private void performRemoval(ListItem item, ListItem parent) {
        if (item.next() == null) {
            if (parent.next() == item) parent.setNext(item.previous());
            else if (parent.previous() == item) parent.setPrevious(item.previous());
            else this.root = item.previous();
        }
        else if (item.previous() == null) {
            if (parent.next() == item) parent.setNext(item.next());
            else if (parent.previous() == item) parent.setPrevious(item.next());
            else  this.root = item.next();
        }
        else {
            ListItem current = item;
            while (current.next() != null) current = current.next();
            current.setNext(item.previous());
            if (parent.next() == item) parent.setNext(item.next());
            else parent.setPrevious(item.next());
        }
    }

    @Override
    public void traverse(ListItem root) {
        if (root == null)  System.out.println("The list is empty");
        else {
            while (root != null) {
                System.out.println(root.getValue());
                root = root.next();
            }
        }
    }
}

// SearchTree (concrete class)
class SearchTree implements NodeList {
    private ListItem root = null;

    public SearchTree(ListItem root) {
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return root;
    }

    @Override
    public boolean addItem(ListItem newItem) {
        if (this.root == null) {
            this.root = newItem;
            return true;
        }

        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparison = currentItem.compareTo(newItem);
            if (comparison < 0) {
                if (currentItem.next() != null) currentItem = currentItem.next();
                else {
                    currentItem.setNext(newItem);
                    return true;
                }
            } else if (comparison > 0) {
                if (currentItem.previous() != null) currentItem = currentItem.previous();
                else {
                    currentItem.setPrevious(newItem);
                    return true;
                }
            } else {
                System.out.println(newItem.getValue() + " is already in the tree.");
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        if (item != null) System.out.println("Deleting item " + item.getValue());
        ListItem currentItem = this.root;
        ListItem parentItem = null;

        while (currentItem != null) {
            int comparison = currentItem.compareTo(item);
            if (comparison < 0) {
                parentItem = currentItem;
                currentItem = currentItem.next();
            } else if (comparison > 0) {
                parentItem = currentItem;
                currentItem = currentItem.previous();
            } else {
                performRemoval(currentItem, parentItem);
                return true;
            }
        }
        return false;
    }

    private void performRemoval(ListItem item, ListItem parent) {
        if (item.next() == null) {
            if (parent.next() == item) parent.setNext(item.previous());
            else if (parent.previous() == item) parent.setPrevious(item.previous());
            else this.root = item.previous();
        } else if (item.previous() == null) {
            if (parent.next() == item) parent.setNext(item.next());
            else if (parent.previous() == item) parent.setPrevious(item.next());
            else this.root = item.next();
        } else {
            ListItem current = item;
            while (current.next() != null) {
                current = current.next();
            }
            current.setNext(item.previous());
            if (parent.next() == item) parent.setNext(item.next());
            else parent.setPrevious(item.next());
        }
    }

    @Override
    public void traverse(ListItem root) {
        if (root == null) System.out.println("The tree is empty");
        else {
            if (root.previous() != null) traverse(root.previous());
            System.out.println(root.getValue());
            if (root.next() != null) traverse(root.next());
        }
    }
}