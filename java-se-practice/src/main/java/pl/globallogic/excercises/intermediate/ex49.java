package pl.globallogic.excercises.intermediate;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Song49 {
    private String title;
    private double duration;

    public Song49(String title, double duration) {
        this.title = title;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public double getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Song49 otherSong = (Song49) obj;
        return title.equals(otherSong.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                ", duration=" + duration +
                '}';
    }
}

class Album49 {
    private String name;
    private ArrayList<Song49> songs;

    public Album49(String name) {
        this.name = name;
        this.songs = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public boolean addSong(String title, double duration) {
        if (findSong(title) == null) {
            songs.add(new Song49(title, duration));
            return true;
        }
        return false;
    }

    public Song49 findSong(String title) {
        for (Song49 song : songs) if (song.getTitle().equals(title)) return song;
        return null;
    }

    public Song49 findSong(int trackNumber) {
        int index = trackNumber - 1;
        if (index >= 0 && index < songs.size()) return songs.get(index);
        return null;
    }

    public List<Song49> getSongs() {
        return songs;
    }
}

class Playlist49 {
    private LinkedList<Song49> playlist;

    public Playlist49() {
        this.playlist = new LinkedList<>();
    }

    public boolean addSong(Song49 song) {
        if (!playlist.contains(song)) {
            playlist.add(song);
            return true;
        }
        return false;
    }

    public List<Song49> getPlaylist() {
        return playlist;
    }
}

public class ex49 {
    public static void main(String[] args) {
        Album49 album1 = new Album49("Album 1");
        Album49 album2 = new Album49("Album 2");

        album1.addSong("Song 1", 3.5);
        album1.addSong("Song 2", 4.2);
        album2.addSong("Track 1", 2.8);
        album2.addSong("Track 2", 3.4);

        Playlist49 playlist = new Playlist49();

        playlist.addSong(album1.findSong("Song 1"));
        playlist.addSong(album1.findSong(2));
        playlist.addSong(album2.findSong(1));
        playlist.addSong(album2.findSong("Track 2"));

        for (Song49 song : playlist.getPlaylist()) System.out.println("Song: " + song.getTitle() + ", Duration: " + song.getDuration());
    }
}
