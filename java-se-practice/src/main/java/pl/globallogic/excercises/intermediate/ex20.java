package pl.globallogic.excercises.intermediate;

public class ex20 {

    public static void main(String[] args) {
        System.out.println(hasSameLastDigit(41, 22, 71));
        System.out.println(hasSameLastDigit(23, 32, 42));
        System.out.println(hasSameLastDigit(9, 99, 999));

        System.out.println(isValid(10));
        System.out.println(isValid(468));
        System.out.println(isValid(1051));
    }
    public static boolean hasSameLastDigit(int num1, int num2, int num3) {
        if (!isValid(num1) || !isValid(num2) || !isValid(num3)) return false;

        int lDigit1 = num1 % 10;
        int lDigit2 = num2 % 10;
        int lDigit3 = num3 % 10;

        return lDigit1 == lDigit2 || lDigit1 == lDigit3 || lDigit2 == lDigit3;
    }

    public static boolean isValid(int number) {
        return number >= 10 && number <= 1000;
    }

}
