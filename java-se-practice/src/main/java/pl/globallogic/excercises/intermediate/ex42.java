package pl.globallogic.excercises.intermediate;

import java.util.Scanner;

public class ex42 {

    public static void main(String[] args) {
        int intsSize = readInteger();
        int[] intsArray = readElements(intsSize);
        int min = findMin(intsArray);
        System.out.println("Minimum value in array is: " + findMin(intsArray));
    }

    public static int readInteger() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of elements: ");
        int num = scanner.nextInt();
        scanner.nextLine();
        return num;
    }

    public static int[] readElements(int count) {
        Scanner scanner = new Scanner(System.in);
        int[] intsElements = new int[count];
        for (int i = 0; i < count; i++) {
            System.out.print("Enter element #" + (i + 1) + ": ");
            intsElements[i] = scanner.nextInt();
            scanner.nextLine();
        }
        return intsElements;
    }

    public static int findMin(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int num : array) if (num < min) min = num;
        return min;
    }

}
