package pl.globallogic.excercises.intermediate;

public class ex43 {

    public static void main(String[] args) {
        int[] ogArray = new int[] {1, 2, 3, 4, 5};
        System.out.println("\nArray: ");
        printArray(ogArray);
        reverse(ogArray);
        System.out.println("\nReversed array: ");
        printArray(ogArray);
    }

    public static void reverse(int[] array) {
        int length = array.length;
        int[] revArray = new int[length];
        for (int i = 0; i < length; i++) revArray[i] = array[length -i - 1];
        System.arraycopy(revArray, 0, array, 0, length);
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if (i < array.length - 1) System.out.print(", ");
        }
    }

}
