package pl.globallogic.lessons.lesson2;

import java.util.Objects;
import java.util.Scanner;

public class ControlFlowSandbox {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wat is your name?");
        String name = scanner.next();
        //String greeting = "Hello " + name;

        String greeting = "";
        if (Objects.equals(name, "Maja")){
            greeting = "Czesc, " + name;
        }
        else if (Objects.equals(name, "John_Xina")){
            greeting = "Bing czeko, " + name;
        }
        else {
            greeting = "Hello there, " + name;
        }
        System.out.println(greeting);
    }
}
